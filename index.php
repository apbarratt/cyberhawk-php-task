<html>
  <head>
    <title>Cyberhawk PHP Task</title>
    <style>
      body {
        font-family: sans-serif;
        margin: 0;
      }
      .part-list {
        display: grid;
        grid-template-columns: repeat(auto-fill, 150px);
        grid-gap: 15px;
        justify-content: space-between;
        margin: 15px;
      }
      .part {
        background: #fed11a;
        color: #fff;
        border-radius: 100%;
        height: 150px;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 18px;
        text-align: center;
        transition: all 0.3s;
      }
      .part:hover {
        background: #FFEB34;
        transform: scale(1.1);
      }
    </style>
  </head>
  <body>
      <div class="part-list">
        <?php
          for($i = 1; $i <= 100; $i++) {
        ?>
          <div class="part" title="Part <?php echo $i; ?>">
            <?php
              if($i % 3 === 0 && $i % 5 === 0) {
                echo 'Coating Damage and Lightning Strike';
              } elseif($i % 3 === 0) {
                echo 'Coating Damage';
              } elseif ($i % 5 === 0) {
                echo 'Lightning Strike';
              } else {
                echo $i;
              }
            ?>
          </div>
        <?php
          }
        ?>
      </div>
  </body>
</html>